package models

type EmployeeModel struct {
	Id        string
	FirtsName string
	LastName  string
	Age       string
	Salary    int
}

func EmployeeInit(theId string, theFirtsName string, theLastName string, theAge string, theSalary int) EmployeeModel {
	emp := EmployeeModel{
		Id:        theId,
		FirtsName: theFirtsName,
		LastName:  theLastName,
		Age:       theAge,
		Salary:    theSalary,
	}
	return emp
}

type Response_Discription struct {
	Description string
}
