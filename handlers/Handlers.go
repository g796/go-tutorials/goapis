package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"goapis.com/services"
	"goapis.com/utils"
)

func Router() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc(utils.PATH_HOME+"employee", getEmployeeAll).Methods(utils.HTTP_METHOD_GET)
	r.HandleFunc(utils.PATH_HOME+"employee/{id}", getEmployeeById).Methods(utils.HTTP_METHOD_GET)
	r.HandleFunc(utils.PATH_HOME+"employee", inSertEmployeeById).Methods(utils.HTTP_METHOD_POST)
	r.HandleFunc(utils.PATH_HOME+"employee", updateEmployeeById).Methods(utils.HTTP_METHOD_PUT)
	r.HandleFunc(utils.PATH_HOME+"employee/{id}", deleteEmployeeById).Methods(utils.HTTP_METHOD_DELETE)
	return r
}

func getEmployeeAll(w http.ResponseWriter, request *http.Request) {
	w.Header().Set(utils.CONTENT_TYPE, utils.APPLICATION_JSON)
	response := services.GetEmployeeAllServices()
	if len(response) != 0 {
		json.NewEncoder(w).Encode(response)
	} else {
		json.NewEncoder(w).Encode(utils.GetDiscription(utils.WARNING_NOT_FOUND))
	}
}

func getEmployeeById(w http.ResponseWriter, request *http.Request) {
	w.Header().Set(utils.CONTENT_TYPE, utils.APPLICATION_JSON)
	vars := mux.Vars(request)
	id, boo := vars["id"]
	if boo {
		response, boo := services.GetEmployeeByIdServices(id)
		if boo {
			json.NewEncoder(w).Encode(response)
		} else {
			json.NewEncoder(w).Encode(utils.GetDiscription(utils.WARNING_NOT_FOUND))
		}
	}
}

func inSertEmployeeById(w http.ResponseWriter, request *http.Request) {
	w.Header().Set(utils.CONTENT_TYPE, utils.APPLICATION_JSON)
	if request.Method == utils.HTTP_METHOD_POST {
		body, err := ioutil.ReadAll(request.Body)
		if err != nil {
			utils.GetDiscription(utils.ConvertErrorToString(err))
		} else {
			payload := utils.ConvertBypeToEmployeeModel(body)
			response := services.InsertEmployeeByIdServices(payload)
			json.NewEncoder(w).Encode(response)
		}
	}
}

func updateEmployeeById(w http.ResponseWriter, request *http.Request) {
	w.Header().Set(utils.CONTENT_TYPE, utils.APPLICATION_JSON)
	if request.Method == utils.HTTP_METHOD_PUT {
		body, err := ioutil.ReadAll(request.Body)
		if err != nil {
			utils.GetDiscription(utils.ConvertErrorToString(err))
		} else {
			payload := utils.ConvertBypeToEmployeeModel(body)
			response := services.UpdateEmployeeByIdServices(payload)
			json.NewEncoder(w).Encode(response)
		}
	}
}

func deleteEmployeeById(w http.ResponseWriter, request *http.Request) {
	w.Header().Set(utils.CONTENT_TYPE, utils.APPLICATION_JSON)
	vars := mux.Vars(request)
	id, boo := vars["id"]
	if boo {
		json.NewEncoder(w).Encode(services.DeleteEmployeeByIdServices(id))
	}
}
