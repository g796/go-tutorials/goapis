package services

import (
	"goapis.com/models"
	"goapis.com/utils"
)

var empData = make(map[string]models.EmployeeModel)

func MockEmployee() {
	nex1 := utils.ConvertIntToString(utils.NextInt())
	nex2 := utils.ConvertIntToString(utils.NextInt())
	empData[nex1] = models.EmployeeInit(nex1, "Mamee", "Naja", "25", 15000)
	empData[nex2] = models.EmployeeInit(nex2, "Malee", "Naja", "25", 25000)
}

func GetEmployeeByIdServices(id string) (models.EmployeeModel, bool) {
	response, boo := empData[id]
	return response, boo
}

func GetEmployeeAllServices() map[string]models.EmployeeModel {
	return empData
}

func InsertEmployeeByIdServices(payload models.EmployeeModel) models.Response_Discription {
	fName := payload.FirtsName
	lName := payload.LastName
	age := payload.Age
	salary := payload.Salary
	if fName != "" && lName != "" && age != "" && salary != 0 {
		nex := utils.ConvertIntToString(utils.NextInt())
		empData[nex] = models.EmployeeInit(nex, fName, lName, age, salary)
		return utils.GetDiscription(utils.WARNING_INSERT_SCCCESS)
	} else {
		return utils.GetDiscription(utils.WARNING_INSERT_NOT_SCCCESS)
	}
}

func UpdateEmployeeByIdServices(payload models.EmployeeModel) models.Response_Discription {
	_, boo := empData[payload.Id]
	if boo {
		empData[payload.Id] = models.EmployeeModel{
			Id:        payload.Id,
			FirtsName: payload.FirtsName,
			LastName:  payload.LastName,
			Age:       payload.Age,
			Salary:    payload.Salary,
		}
		return utils.GetDiscription(utils.WARNING_UPDATE_SCCCESS)
	} else {
		return utils.GetDiscription(utils.WARNING_NOT_FOUND)
	}
}

func DeleteEmployeeByIdServices(id string) models.Response_Discription {
	_, boo := empData[id]
	if boo {
		delete(empData, id)
		return utils.GetDiscription(utils.WARNING_DELETE_SCCCESS)
	} else {
		return utils.GetDiscription(utils.WARNING_NOT_FOUND)
	}
}
