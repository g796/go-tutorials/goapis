package utils

const (
	PATH_HOME        string = "/api/"
	CONTENT_TYPE     string = "Content-Type"
	APPLICATION_JSON string = "application/json"

	HTTP_METHOD_GET    string = "GET"
	HTTP_METHOD_POST   string = "POST"
	HTTP_METHOD_PUT    string = "PUT"
	HTTP_METHOD_DELETE string = "DELETE"

	WARNING_NOT_FOUND          = "Data Not Fount"
	WARNING_INSERT_SCCCESS     = "Insert Employee is success"
	WARNING_INSERT_NOT_SCCCESS = "Insert Employee is not success"
	WARNING_UPDATE_SCCCESS     = "Update Employee is success"
	WARNING_DELETE_SCCCESS     = "Delete Employee is success"
)