package utils

import (
	"encoding/json"
	"strconv"

	"goapis.com/models"
)

func GetDiscription(str string) models.Response_Discription {
	response := models.Response_Discription{
		Description: str,
	}
	return response
}

var NextInt = intSeq()

func intSeq() func() int {
	i := 1000
	return func() int {
		i++
		return i
	}
}

func ConvertIntToString(num int) string {
	response := strconv.Itoa(num)
	return response
}

func ConvertErrorToString(e error) string {
	return e.Error()
}

func ConvertBypeToEmployeeModel(byt []byte) models.EmployeeModel {
	var obj models.EmployeeModel
	if err := json.Unmarshal(byt, &obj); err != nil {
		panic(err)
	}
	return obj
}
