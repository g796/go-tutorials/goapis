package main

import (
	"log"
	"net/http"

	"goapis.com/handlers"
	"goapis.com/services"
)

func init() {
	services.MockEmployee()
}

func main() {
	httpListenServe()
}

func httpListenServe() {

	log.Print("Starting the service")
	port := "9000"
	router := handlers.Router()
	log.Println("The service is ready to listen and serve.")
	log.Fatal(http.ListenAndServe(":"+port, router))
}
